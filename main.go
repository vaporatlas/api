package main

import (
	"fmt"
	"os"

	"github.com/go-pg/pg"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	dbURL := os.Getenv("DATABASE_URL")
	if dbURL == "" {
		dbURL = databaseURL
	}
	port := os.Getenv("PORT")
	if port == "" {
		port = "1312"
	}

	opt, err := pg.ParseURL(dbURL)
	if err != nil {
		panic(err)
	}

	db := pg.Connect(opt)
	//db.AddQueryHook(dbLogger{})
	defer db.Close()

	err = createSchema(db)

	vm, err := loadVideos(db)
	if err != nil {
		panic(err)
	}

	api := API{db, vm}
	// Echo instance
	e := echo.New()
	e.HideBanner = true

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"http://localhost:3000", "https://vaporatlas.gitlab.io"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))

	// Routes
	e.GET("/random", api.random)

	// Start server
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%s", port)))
}
