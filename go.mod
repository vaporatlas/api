module gitlab.com/vaporatlas/api

// +heroku goVersion go1.14
go 1.14

require (
	github.com/go-pg/pg v8.0.7+incompatible
	github.com/go-pg/pg/v10 v10.0.0-beta.9 // indirect
	github.com/labstack/echo/v4 v4.1.16
	github.com/satori/go.uuid v1.2.0
)
