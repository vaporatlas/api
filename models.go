package main

import uuid "github.com/satori/go.uuid"

// VideoInfo contains infos to a video.
type VideoInfo struct {
	tableName   struct{}  `sql:"videos"`
	ID          uuid.UUID `json:"-" pg:"id, type:uuid"`
	VideoID     string    `json:"videoId" pg:"video_id"`
	Title       string    `json:"title" pg:"title"`
	Photo       string    `json:"photo" pg:"photo"`
	Description string    `json:"description" pg:"description"`
	Views       int       `json:"views" pg:"views"`
}
