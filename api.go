package main

import (
	"net/http"
	"time"

	"github.com/go-pg/pg"
	"github.com/labstack/echo/v4"
)

// API contains the database which can be accessed inside the handlers.
type API struct {
	db *pg.DB
	vm videoManager
}

// APIResponse provides a shared format for all responses to external requests to the API.
type APIResponse struct {
	Message   string      `json:"message" xml:"message"`
	Timestamp time.Time   `json:"timestamp" xml:"timestamp"`
	Data      interface{} `json:"data" xml:"data"`
}

func newAPIResponse(message string, data interface{}) APIResponse {
	return APIResponse{Message: message, Data: data, Timestamp: time.Now()}
}

func (api API) random(c echo.Context) error {
	videos, err := api.vm.getRandomVideo(api.db)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, "No videos found")
	}

	resp := newAPIResponse("OK", videos)

	return c.JSON(http.StatusOK, resp)
}
