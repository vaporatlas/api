package main

import (
	"errors"
	"fmt"
	"math/rand"
	"time"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

type dbLogger struct{}

func (d dbLogger) BeforeQuery(q *pg.QueryEvent) {
	return
}

func (d dbLogger) AfterQuery(q *pg.QueryEvent) {
	fmt.Println(q.FormattedQuery())
	return
}

// createSchema creates database schema for User and Story models.
func createSchema(db *pg.DB) error {
	models := []interface{}{
		//(*VideoInfo)(nil),
	}

	for _, model := range models {
		err := db.Model(model).CreateTable(&orm.CreateTableOptions{
			Temp: true,
		})
		if err != nil {
			return err
		}
	}
	return nil
}

type videoManager struct {
	videos []VideoInfo
}

func loadVideos(db *pg.DB) (videoManager, error) {
	var videos []VideoInfo
	err := db.Model(&videos).Select()
	if err != nil {
		panic(err)
	}

	videoManager := videoManager{videos}

	return videoManager, err
}

func (vm *videoManager) getRandomVideo(db *pg.DB) (*VideoInfo, error) {
	if len(vm.videos) == 0 {
		return nil, errors.New("No videos found")
	}

	rand.Seed(time.Now().UnixNano())

	idx := rand.Intn(len(vm.videos) + 1)

	return &vm.videos[idx], nil
}
